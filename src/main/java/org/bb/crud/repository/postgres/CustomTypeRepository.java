package org.bb.crud.repository.postgres;

import org.bb.crud.model.common.PagedRequestDto;
import org.springframework.data.domain.Page;

public interface CustomTypeRepository<T> {
	
	Page<T> getAnswerForAggregation(PagedRequestDto aggregationRequest, Class<T> outputType);
}