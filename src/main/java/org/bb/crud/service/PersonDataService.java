package org.bb.crud.service;

import java.util.ArrayList;
import java.util.List;

import org.bb.crud.model.mysql.Person;
import org.bb.crud.repository.postgres.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonDataService {

	@Autowired
	private PersonRepository personRepository;
	
	public List<Person> getAllPerson() {
		Iterable<Person> allRecords = personRepository.findAll();
		List<Person> personList = new ArrayList<Person>();
		allRecords.forEach(personList::add);
		return personList;
	}
}
