package org.bb.crud.api;

import java.util.List;

import org.bb.crud.model.mysql.Person;
import org.bb.crud.service.PersonDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/person")
public class PersonRestController {

	Logger log = LoggerFactory.getLogger(PersonRestController.class);

	@Autowired
	private PersonDataService personDataService;

	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Person> getAllPerson() {
		return personDataService.getAllPerson();
	}
}
