package org.bb.crud.model.common;

import java.util.Date;

public class ResponseDto {

	protected String code;
	protected String status;
	protected String message;
	protected Date timestamp;
	protected String path;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public ResponseDto() {
		super();
	}

	public ResponseDto(String status) {
		super();
		this.status = status;
	}

	public ResponseDto(String status, String message) {
		super();
		this.status = status;
		this.message = message;
	}	
}
