package org.bb.crud.model.common;

public class ErrorResponseDto extends ResponseDto {
	
	private String stackTrace;

	public String getStackTrace() {
		return stackTrace;
	}

	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}
}
