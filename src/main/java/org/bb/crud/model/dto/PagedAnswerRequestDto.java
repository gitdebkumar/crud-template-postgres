package org.bb.crud.model.dto;

import org.bb.crud.model.common.PagedRequestDto;

public class PagedAnswerRequestDto extends PagedRequestDto {

	private String questionSetId;
	
	public String getQuestionSetId() {
		return questionSetId;
	}

	public void setQuestionSetId(String questionSetId) {
		this.questionSetId = questionSetId;
	}

	public PagedAnswerRequestDto() {
		super();
	}

	public PagedAnswerRequestDto(int pageNo, int pageSize) {
		super(pageNo, pageSize);
	}
}
